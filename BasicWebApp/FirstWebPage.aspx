﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FirstWebPage.aspx.cs" Inherits="BasicWebApp.FirstWebPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script type="text/javascript" src="error_message.js"></script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="height: 311px">
            Simple Calculator<br />
            <br />
            <br />
            Layar Input:<br />
            <asp:TextBox ID="TextBox1" runat="server" Width="306px"></asp:TextBox>
            <br />
            <asp:Button ID="Button11" runat="server" Text="7" OnClick="Button11_Click" />
            <asp:Button ID="Button12" runat="server" Text="8" OnClick="Button12_Click" />
            <asp:Button ID="Button13" runat="server" Text="9" OnClick="Button13_Click" />
            <asp:Button ID="Button14" runat="server" OnClick="Button14_Click" Text=":" />
            <br />
            <asp:Button ID="Button8" runat="server" Text="4" OnClick="Button8_Click" />
            <asp:Button ID="Button9" runat="server" Text="5" OnClick="Button9_Click" />
            <asp:Button ID="Button10" runat="server" Text="6" OnClick="Button10_Click" />
            <asp:Button ID="Button15" runat="server" OnClick="Button15_Click" Text="x" />
            <br />
            <asp:Button ID="Button5" runat="server" Text="1" OnClick="Button5_Click" />
            <asp:Button ID="Button6" runat="server" Text="2" OnClick="Button6_Click" style="width: 21px" />
            <asp:Button ID="Button7" runat="server" Text="3" OnClick="Button7_Click" />
            <asp:Button ID="Button16" runat="server" OnClick="Button16_Click" Text="-" />
            <br />
            <asp:Button ID="Button4" runat="server" OnClick="Button4_Click" Text="0" />
            <asp:Button ID="Button17" runat="server" OnClick="Button17_Click" Text="(" />
            <asp:Button ID="Button18" runat="server" OnClick="Button18_Click" Text=")" />
            <asp:Button ID="Button19" runat="server" OnClick="Button19_Click" Text="+" />
            <br />
            <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="C" />
            <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="CE" />
            <asp:Button ID="Button1" runat="server" Text="Enter" OnClick="Button1_Click" Width="76px" />
        </div>
    </form>
</body>
</html>
